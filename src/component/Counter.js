import { useEffect, useState } from "react";

const Counter= () => {

    const [compteur, setCompteur] = useState(0);
    const [message, setMessage] = useState('Pas de compteur');

    const clickHandler = (fonction) => setCompteur(fonction);

    useEffect(
        () => {
            compteur && setMessage(`Compteur : ${compteur}`)
    }, [compteur])
    
    return (
        <div>
            {message}
            <button onClick={() => clickHandler(x => x + 1)}>Plus</button>
            <button onClick={() => clickHandler(x => x - 1)}>Moins</button>
            <button onClick={() => clickHandler(x => x + 2)}>Plus deux</button>
        </div>
        )
}


export {Counter};